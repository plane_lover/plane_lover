import dash
from dash import dcc, html, Input, Output, callback, dash_table
import pages.get_data

df = pages.get_data.get_data_avion()
df_doublon = df.drop_duplicates(['Source'])

dash.register_page(__name__)


# Zone des fonctions:-----------------------------------------------------------------
def df_source(dataframe, source):
    index_source = dataframe[dataframe["Source"] != source].index
    df_f_source = dataframe.drop(index_source)
    return df_f_source


def df_destination(dataframe, source, destination):
    # Fonction df source
    df_d_source = df_source(dataframe, source)
    # Fonction df destination
    index_destination = df_d_source[df_d_source["Destination"] != destination].index
    df_f_destination = df_d_source.drop(index_destination)
    return df_f_destination


def moyenne(dataframe, source, destination):
    """ Affiche la durée moyenne et le prix moyen d’un voyage entre les deux """
    result = []
    # Source
    df_m_cource = df_source(dataframe, source)
    print(df_m_cource.loc[:, ["Source", "Destination"]])
    # Destination
    df_m_destination = df_destination(dataframe, source, destination)
    print(df_m_destination.loc[:, ["Source", "Destination"]])
    # Calcul de moyenne de temps et de prix
    result.append(df_m_destination["Duration_minutes"].mean())
    result.append(df_m_destination["Price"].mean())
    print(result)
    print(df.columns)
    return result


# Layout Dash de la zone graphique:----------------------------------------------------
layout = html.Div([
    html.H1('Duree et prix moyen entre deux aeroport'),
    html.Hr(),
    html.P('Une vue avec 2 selects. Dans chacun la liste des aéroports. '
           'Quand je sélectionne les deux, affiche la durée moyenne et'
           ' le prix moyen d’un voyage entre les deux.'),
    html.Hr(),
    html.Div([
        # Affichage sélection 1
        html.P(id='pandas-output-container-1'),
        # Affichage sélection 2
        html.P(id='pandas-output-container-2'),
        html.Hr(),
        # Affichage temps et prix moyen
        # html.P(id='pandas-output-container-5'),
        html.Div(id='table-simple'),
        html.Hr()
    ]),
    html.P('Sélection 1:'),
    dcc.Dropdown(df_doublon["Source"],
                 id='pandas-dropdown-1',
                 placeholder="Select a city",
                 searchable=False,
                 clearable=False),
    html.P('Sélection 2:'),
    dcc.Dropdown(df_doublon["Destination"],
                 id='pandas-dropdown-2',
                 placeholder="Select a city",
                 searchable=False,
                 clearable=False)
    ])


# Callback du dropdown de la sélection 1 :----------------------------------------------
@callback(
    Output('pandas-output-container-1', 'children'),
    Input('pandas-dropdown-1', 'value')
)
def update_output(value):
    return f'Sélection 1: {value}'


# Callback du dropdown de la sélection 2 :----------------------------------------------
@callback(
    Output('pandas-output-container-2', 'children'),
    Input('pandas-dropdown-2', 'value')
)
def update_output(value):
    return f'Sélection 2: {value}'


# Callback du temps et prix moyen:-----------------------------------------------------
@callback(
    Output('table-simple', 'children'),
    Input('pandas-dropdown-1', 'value'),
    Input('pandas-dropdown-2', 'value')
)
def update_output(value_1, value_2):
    result = moyenne(df, str(value_1), str(value_2))
    # return f'Temps moyen: {result[0] / 60} h et Prix moyen: {result[1]} $'
    return dash_table.DataTable(
            columns=([{'id': 'col1', 'name': 'Temps en H', "type": "numeric", "format": {"specifier": ".2f"}},
                      {'id': 'col2', 'name': 'Prix en ₹', "type": "numeric", "format": {"specifier": ".2f"}}]),
            data=[{"col1": result[0] / 60, 'col2': result[1]}])


# Zone de test:-------
if __name__ == '__main__':
    print("Teste du script:")
    print(df.columns)
