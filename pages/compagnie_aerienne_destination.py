import dash
from dash import html, dcc, dash_table, callback, Output, Input
from .get_data import get_data_avion, get_data_iata
import pandas as pd

df = get_data_avion()
df2= df.copy()
df2['Route'] = df2['Route'].str.split(pat=' → ')
df_iata = get_data_iata()

dash.register_page(__name__)

layout = html.Div([
    html.H1('Compagnies aeriennes et leurs destinations'),
    dcc.Dropdown(df['Airline'].unique(), value=df['Airline'][0], id='dropdown'),
    html.Div(id='tbl')
])
@callback(
    Output('tbl','children'),
    Input('dropdown', 'value')

)
def get_destination(airline):

    t_df = df2.loc[df['Airline'] == airline, 'Route']
    t_df.dropna(inplace=True)
    t_df2 = t_df.explode(ignore_index=True)
    t_df2.drop_duplicates(inplace=True)
    result_df = pd.merge(df_iata, t_df2, left_on='index', right_on='Route', how='inner')[['index','municipality']]
    return dash_table.DataTable(
        data= result_df.to_dict('records'),
        columns=[{"name": i, "id": i} for i in result_df.columns]
    )


