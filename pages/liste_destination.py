import dash

from dash import html, dcc, dash_table
import plotly.express as px
from .get_data import get_data_avion
import pandas as pd

df = get_data_avion()


average_prices = df.groupby('Destination')['Price'].mean().reset_index()
sorted_destinations = average_prices.sort_values(by='Price', ascending=False)

dash.register_page(__name__)

layout = html.Div([
    html.H1('Liste des destinations de la plus cher a la moins cher'),
    dash_table.DataTable(sorted_destinations.to_dict('records'), page_size=10),
    dcc.Graph(
        id='prix-moyen-destinations',
        figure=px.bar(sorted_destinations, x='Destination', y='Price', title='prix moyen par destination')
    )
])

if __name__ == '__main__':
    df = get_data_avion()
    print(df)