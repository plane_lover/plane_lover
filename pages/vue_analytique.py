import dash
import folium
import pandas as pd
import plotly.express as px
from dash import html, dcc, dash_table
from .get_data import get_data_iata, get_data_avion

# Créations du DF global: ----------------------------------------------------------
df_iata = get_data_iata()
df_avion = get_data_avion()
df_avion.dropna(subset='Route', inplace=True)
df_avion.reset_index(inplace=True)
df_avion['Route'] = df_avion['Route'].str.split(' → ')
dash.register_page(__name__)


# Créations des fonctions: ----------------------------------------------------------
def generer_map():
    def make_lines(x, ):
        folium.PolyLine([(x['latitude_d'], x['longitude_d']), (x['latitude_a'], x['longitude_a'])], color='blue',
                        weight=int(1 * (1 + x['normal'] * 10))).add_to(map)

    def make_markers(x):
        folium.Marker(
            location=[x['latitude'], x['longitude']],
            tooltip=x.municipality,
            popup=x.index,
            icon=folium.Icon(icon="cloud"),
        ).add_to(map)

    df_iata[['longitude', 'latitude']] = df_iata['coordinates'].str.split(',', expand=True).astype(float)
    df_iata.set_index('index', inplace=True)
    tuples_sub_travel = [tuple(sorted([df_avion.loc[i, 'Route'][j], df_avion.loc[i, 'Route'][j + 1]])) for i, route in
                         enumerate(df_avion['Route']) for j, etape in enumerate(df_avion.loc[i, 'Route'][1:])]
    index = pd.MultiIndex.from_tuples(tuples_sub_travel, names=["departure", "arrival"])
    series = index.value_counts()
    df_trajet = series.to_frame()
    df_trajet['normal'] = (series - series.min()) / (series.max() - series.min())
    df_trajet.reset_index(inplace=True)
    df_trajet = df_trajet.join(df_iata[['longitude', 'latitude']].add_suffix('_d'), on=['departure'])
    df_trajet = df_trajet.join(df_iata[['longitude', 'latitude']].add_suffix('_a'), on=['arrival'])
    map = folium.Map(location=(df_iata['latitude'].mean(), df_iata['longitude'].mean()), zoom_start=5)
    df_iata.apply(make_markers, axis=1)
    df_trajet.apply(make_lines, axis=1)
    map.save("data/map.html")


def destionation_la_plus_frequente():
    all_airports = [airport for route in df_avion['Route'] for airport in route[1:]]
    airport_counts = pd.Series(all_airports).value_counts()
    most_common_airport = airport_counts.idxmax()
    frequency = airport_counts.max()
    return (airport_counts, most_common_airport, frequency)


def fig_destination_la_plus_frequent(airport_counts):
    return px.bar(airport_counts, x=airport_counts.index, y=airport_counts.values,
                  labels={'x': 'Aeroport', 'y': 'Fréquence'},
                  title='Fréquence des aeroports')


def source_la_plus_frequente():
    # Calculer l'aéroport de départ le plus récurrent
    return df_avion['Source'].mode()[0]


def correspondance_la_plus_frequente():
    series = df_avion['Route']
    index = pd.Index([correspondance for route in series for correspondance in route[1:-1]])
    return index.value_counts()


def destination_cochin_depart_moyenne_temps_n_correspondance():
    temp_df = df_avion.loc[df_avion['Destination'] == 'Cochin', ['Source', 'Duration_minutes', 'Route']]
    temp_df['ncorrespondance'] = [len(route[1:-1]) for route in temp_df['Route']]
    temp_df.drop(columns='Route', inplace=True)
    temp_df = temp_df.groupby('Source').mean()
    temp_df.reset_index(inplace=True)
    return temp_df


def depart_cochin_depart_moyenne_temps_n_correspondance():
    temp_df = df_avion.loc[df_avion['Source'] == 'Cochin', ['Destination', 'Duration_minutes', 'Route']]
    temp_df['ncorrespondance'] = [len(route[1:-1]) for route in temp_df['Route']]
    temp_df.drop(columns='Route', inplace=True)
    temp_df = temp_df.groupby('Destination').mean()
    temp_df.reset_index(inplace=True)
    return temp_df


def histogramme_heure_depart_arrive():
    # Créer un histogramme pour les heures de départ
    fig = px.histogram(df_avion, x=df_avion['departure_date'].dt.hour, title='Nombre de départs par heure')
    fig.update_xaxes(title='Heure de départ')
    fig.update_yaxes(title='Nombre de départs')
    # Ajouter le nombre d'arrivées
    arrival_counts = df_avion['arrival_date'].dt.hour.value_counts().sort_index()
    fig.add_bar(x=arrival_counts.index, y=arrival_counts.values, name='Arrivées')
    return fig


def flight_counts():
    # Compter le nombre de vols par compagnie aérienne
    return df_avion['Airline'].value_counts()


def exo8():
    df_copy8 = df_avion.copy()
    df_copy8['Route'] = ['-'.join(route) for route in df_copy8['Route']]
    # Mise en forme Graph
    df_8 = mise_en_forme_exo8(df_copy8, ['Route', 'Duration_minutes'], "Route")
    fig8 = trajet_long(df_8)
    # Mise en forme texte
    max_duration = (df_copy8["Duration_minutes"].max())
    max_route = df_copy8.loc[df_copy8["Duration_minutes"].idxmax(), "Route"]
    return (max_duration, max_route, fig8)


def mise_en_forme_exo8(dataframe, col, group_by):
    df = dataframe[col]
    df_bis = df.groupby(group_by).max()
    return df_bis


def trajet_long(dataframe):
    return px.histogram(dataframe,
                        x=dataframe.index,
                        y="Duration_minutes",
                        color="Duration_minutes",
                        color_discrete_sequence=["blue"])


# Appel des fonctions:  ------------------------------------------------------------
generer_map()
airport_counts, most_common_airport, frequency = destionation_la_plus_frequente()
correspondances = correspondance_la_plus_frequente()
destination_cochin = destination_cochin_depart_moyenne_temps_n_correspondance()
depart_cochin = depart_cochin_depart_moyenne_temps_n_correspondance()
max_duration, max_route, fig8 = exo8()

# Affichage du Layout: -------------------------------------------------------------
layout = html.Div([
    html.Hr(),
    html.Div([
        html.H3("Aeroport de destination le plus recurrent"),
        html.Label(
            f"L'Aéroport de destination le plus récurrent est: '{most_common_airport}' avec une fréquence de {frequency} fois."),
        dcc.Graph(figure=fig_destination_la_plus_frequent(airport_counts))
    ]),
    html.Div([
        html.H3("Aéroport de départ le plus récurrent :"),
        html.P(source_la_plus_frequente()),
        dcc.Graph(
            id='graph_aeroport_recurrent',
            figure=px.histogram(df_avion, x='Source', y='Price', title='Aéroport de départ le plus récurrent:',
                                color='Source')
        )
    ]),
    html.Div([
        html.H3("Aeroport qui sert le plus souvent de correspondance :"),
        html.P(f" {correspondances.idxmax()} avec {correspondances.loc[correspondances.idxmax()]} correspondances."),
        dcc.Graph(
            id='graph_aeroport_recurrent',
            figure=px.histogram(correspondances, x=correspondances.index, y='count',
                                title='Aéroport qui sert le plus souvent de correspondance:')
        )
    ]),
    html.Div([
        html.H2('Exo 8: Le voyage le plus long avec sa durée'),
        html.P(f'Le voyage le plus long est {max_route} et est de {max_duration} min'),
        dcc.Graph(figure=fig8)
    ]),
    html.Div([
        html.H3(
            "Destination cochin, liste des aeroport de depart, moyenne du temps de trajet, nombre de correspondance :"),
        dash_table.DataTable(
            data=destination_cochin.to_dict('records'),
            columns=[{"name": i, "id": i} for i in destination_cochin.columns]
        )
    ]),
    html.Div([
        html.H3("Depart cochin, liste des aeroport de depart, moyenne du temps de trajet, nombre de correspondance :"),
        dash_table.DataTable(
            data=depart_cochin.to_dict('records'),
            columns=[{"name": i, "id": i} for i in depart_cochin.columns]
        )
    ]),
    html.Div([
        html.H3("Un histogramme avec les heures en abscisse et le nombre de départs en ordonnée. "),
        dcc.Graph(figure=histogramme_heure_depart_arrive())
    ]),
    html.Div([
        html.H3("Carte de vols"),
        html.Iframe(id='map', srcDoc=open('data/map.html').read(), width='100%', height='600')
    ]),
    html.Div([
        html.H3("Nombre de vols par compagnie aérienne"),
        html.Table([
            html.Thead(html.Tr([html.Th('Compagnie aérienne'), html.Th('Nombre de vols')])),
            html.Tbody([html.Tr([html.Td(airline), html.Td(count)]) for airline, count in flight_counts().items()])
        ]),
    ])
],
    style={'padding': '30px 30px 30px 30px',
           'float': 'center',
           'width': '95%',
           'display': 'inline-block'}
)
