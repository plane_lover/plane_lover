import pandas as pd

def get_data_avion():
    df = pd.read_json('./data/data_avion.json')
    df['arrival_date'] = pd.to_datetime(df['arrival_date'], unit='ms')
    df['departure_date'] = pd.to_datetime(df['date_depart'], unit='ms')
    return df

def get_data_iata():
    df = pd.read_json('./data/iata.json')
    df.reset_index(inplace=True)
    return df

