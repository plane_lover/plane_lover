import pandas as pd


def extract_data(input, output):
    file = ('data/data-avion-654e41bbacd4a359723456.xlsx')
    df = pd.read_excel(input)
    # Conversion de la colonne 'Duration' en minutes sans fonction

    df['Duration_minutes'] = df['Duration'].apply(
        lambda x: sum(int(i[:-1]) * (60 if i[-1] == 'h' else 1) for i in x.split()))

    df['date_depart'] = df['Date_of_Journey'] + ' ' + df['Dep_Time']
    df['date_depart'] = pd.to_datetime(df['date_depart'], dayfirst=True)

    df['date_depart'] = pd.to_datetime(df['date_depart'])
    df['arrival_date'] = df.apply(lambda row: row['date_depart'] + pd.Timedelta(minutes=row['Duration_minutes']),
                                  axis=1)

    # Suppression de la colonne 'colonne_a_supprimer'
    if 'Date_of_Journey' in df:
        df.drop('Date_of_Journey', axis=1, inplace=True)  # Utilisation de la méthode drop

    # Suppression de la colonne 'colonne_a_supprimer'
    if 'Dep_Time' in df:
        df.drop('Dep_Time', axis=1, inplace=True)  # Utilisation de la méthode drop

    # Suppression de la colonne 'colonne_a_supprimer'
    if 'Arrival_Time' in df:
        df.drop('Arrival_Time', axis=1, inplace=True)  # Utilisation de la méthode drop

    df.to_json(output)


if __name__ == "__main__":
    extract_data('./data/data-avion-654e41bbacd4a359723456.xlsx', './data/data_avion.json')
